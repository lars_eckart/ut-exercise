package ee.ut.cs.mobile.utexercise.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ee.ut.cs.mobile.utexercise.R;
import ee.ut.cs.mobile.utexercise.rest.model.GitHubRepository;


public class RepositoryAdapter extends ArrayAdapter<GitHubRepository> {

    private final Context context;
    private final List<GitHubRepository> values;

    public RepositoryAdapter(Context context, List<GitHubRepository> values) {
        super(context, R.layout.rowlayout, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.rowlayout, parent, false);

        // TODO: use butterknife
        // TODO: and ViewHolder

        GitHubRepository repo = values.get(position);

        TextView labelTextView = (TextView) rowView.findViewById(R.id.labelTextView);
        TextView descriptionTextView = (TextView) rowView.findViewById(R.id.descriptionTextView);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon); // unfortunately github repos dont have icons, so we just use app icon
        labelTextView.setText(repo.getFullName());
        descriptionTextView.setText(repo.getDescription());

        return rowView;
    }
}