package ee.ut.cs.mobile.utexercise.rest;

import ee.ut.cs.mobile.utexercise.rest.service.GitHubService;
import retrofit.RestAdapter;

public class RestClient {

    private static final String BASE_URL = "https://api.github.com";

    private GitHubService apiService;

    public RestClient() {

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .build();

        apiService = restAdapter.create(GitHubService.class);
    }

    public GitHubService getGitHubService() {
        return apiService;
    }
}
