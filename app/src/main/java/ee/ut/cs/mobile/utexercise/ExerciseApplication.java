package ee.ut.cs.mobile.utexercise;

import android.app.Application;

import ee.ut.cs.mobile.utexercise.rest.RestClient;


public class ExerciseApplication extends Application {

    private static RestClient restClient;

    @Override
    public void onCreate() {
        super.onCreate();

        restClient = new RestClient();
        // TODO: init ImageLoader
    }

    public static RestClient getRestClient() {
        return restClient;
    }
}