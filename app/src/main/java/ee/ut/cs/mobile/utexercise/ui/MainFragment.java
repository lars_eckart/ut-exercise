package ee.ut.cs.mobile.utexercise.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import butterknife.ButterKnife;
import butterknife.OnClick;
import ee.ut.cs.mobile.utexercise.R;


public class MainFragment extends Fragment {


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Initialize the Fragment.
        setHasOptionsMenu(true);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.inject(this, view);
        if (view == null) {
            return null;
        }

        return view;
    }

    @OnClick(R.id.searchButton)
    protected void searchRepositories(){
        Toast.makeText(getActivity(), "implement me", Toast.LENGTH_SHORT).show();
        // TODO: ExerciseApplication.getRestClient().getGitHubService().getRepositories(.....
        // TODO: use RepositoryAdapter and display the repositories in the listview
        // TODO: load user's avatar and display avatar, login and id in userDetailsLayout (and make it visible on success)

    }
}
