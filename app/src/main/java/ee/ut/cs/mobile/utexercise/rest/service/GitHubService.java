package ee.ut.cs.mobile.utexercise.rest.service;


import ee.ut.cs.mobile.utexercise.rest.model.GitHubUser;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface GitHubService {

    @GET("/users/{userName}")
    public void getUser(@Path("userName") String userName, Callback<GitHubUser> callback);

    @GET("/users/{userName}")
    public GitHubUser getUser(@Path("userName") String userName);

    // TODO: write get method for repositories
}
